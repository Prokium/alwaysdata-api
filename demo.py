#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Demonstration of the Alwaysdata API interface.

Note: Requires the ALWAYSDATA_API_KEY and ALWAYSDATA_ACCOUNT environment
    variables to be set. Raises HTTPError 401 otherwise (Client Error:
    Unauthorized for url).
"""
from alwaysdata_api import Domain, Record, Resource


if __name__ == '__main__':
    # List all domains.
    domains = Domain.list(name='paulkoppen')
    print('\n'.join(map(str, domains)))
    print('-' * 30)
    # Domain(..., date_expiration='2021-09-22T17:38:35', ..., name='paulkoppen.com')
    # ------------------------------

    domain = domains[0]

    # Find DNS records with 'mg' in the name.
    records = Record.list(domain=domain.id, type='NS')
    print('\n'.join(map(str, records)))
    print('-' * 30)
    # Record(..., ttl=300, type='NS', value='dns2.alwaysdata.com')
    # Record(..., ttl=300, type='NS', value='dns1.alwaysdata.com')
    # ------------------------------

    # Print the WHOIS information.
    print(domain.whois())
    print('-' * 30)
    #    Domain Name: PAULKOPPEN.COM
    #    Registry Domain ID: 1569922829_DOMAIN_COM-VRSN
    #    Registrar WHOIS Server: whois.gandi.net
    #    Registrar URL: http://www.gandi.net
    #    Updated Date: 2018-05-29T23:11:08Z
    # ...
    # ------------------------------

    # Create a TXT record in the DNS.
    record = Record(domain=domain.id, type='TXT', name='Hello', value='world!')
    record.post()

    # Find the record.
    records = Record.list(domain=domain.id, value='world!')
    assert len(records) == 1
    record = records[0]
    assert record.type == 'TXT'
    assert record.name == 'hello'

    # Update the record.
    record.value = 'you!'
    record.put()
    record = Record.get(record.id)
    assert record.value == 'you!'

    # Update via PATCH (which sends partial data, i.e. the diff).
    rec2 = Record(**record.__dict__)
    rec2.value = 'again, world.'
    diff = Resource.delta(record, rec2)
    print(diff)     # Notice it has only two fields.
    diff.patch()
    record = Record.get(record.id)
    assert record.value == 'again, world.'
    # Record(href='/v1/record/2993416/', value='again, world.')

    # Delete the DNS record.
    record.delete()

    # Fail to retrieve the record.
    from requests.exceptions import HTTPError
    try:
        record = Record.get(record.id)
        raise Exception('This is bad.')
    except HTTPError as err:
        assert err.response.status_code == 404
        print("Good, we're done.")
    # Good, we're done.
